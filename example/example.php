<?php

$config = \Morilog\Paymand\Config\ConfigBuilder::createBuilder()
    ->enableHighAvailable(2)
    ->build();


$paymand = new \Morilog\Paymand\Paymand($config);


// start_pay.php
try {
    $needs = (new \Morilog\Paymand\Contracts\RequestNeeds(
        new \Morilog\Paymand\Contracts\Money(1000),
        'http://example.com/my_pay'
    ))
        ->with('email', 'morilog.ir@gmail.com')
        ->with('description', 'Buy something');

    $result = $paymand->request($needs);

    // Save data in db
    Database::table('transactions')->insert([
        'transactionId' => $result->getIdentifier(),
        'amount' => $result->getAmount()->getRials(),
        'createdAt' => (new \DateTime())->format('Y-m-d H:i:s'),
        'gateway' => $result->getGatewayName(),
        'status' => 'pending',
    ]);

    http_redirect($result->getPaymentUrl());
} catch (\Morilog\Paymand\Contracts\PaymentException $e) {
    echo $e->getGateway();
    echo $e->getMessage();
}


// verify.php
$transactionId = $_POST['transaction_id'];
$transaction = Database::table('transactions')->find($transactionId);

try {
    $result = $paymand->choose($transaction->gateway)->verify(new VerifyNeeds(
        $transactionId,
        new \Morilog\Paymand\Contracts\Money($transaction->amount),
    ));
    $transaction->status = 'paid';
    $transaction->referenceCode = $result->getReferenceCode();
    $transaction->save();
} catch (\Morilog\Paymand\Contracts\PaymentException $e) {
    $transaction->status = 'failed';
    $transaction->save();
}
