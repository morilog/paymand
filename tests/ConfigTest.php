<?php

namespace Morilog\Paymand\Tests;

use Morilog\Paymand\Config\Config;
use Morilog\Paymand\Config\ConfigBuilder;
use Morilog\Paymand\Config\ProxyConfig;
use PHPUnit\Framework\TestCase;

final class ConfigTest extends TestCase
{
    public function testCreateDefaults()
    {
        $config = ConfigBuilder::buildDefaults();

        $this->assertTrue($config instanceof Config);
        $this->assertTrue($config->getProxyConfig() instanceof ProxyConfig);
        $this->assertFalse($config->getProxyConfig()->isEnabled());
        $this->assertNull($config->getDefaultGateway());
        $this->assertFalse($config->isHighAvailableMode());
    }

    public function testEnableProxy()
    {
        $config = ConfigBuilder::createBuilder()
            ->enableProxy()
            ->setProxyHost('mylocalhost')
            ->build();

        $this->assertTrue($config->getProxyConfig()->isEnabled());
        $this->assertEquals('mylocalhost', $config->getProxyConfig()->getHost());
    }

    public function testEnableHAMode()
    {
        $config = ConfigBuilder::createBuilder()
            ->enableHighAvailable(4)
            ->build();

        $this->assertTrue($config->isHighAvailableMode());
        $this->assertEquals(4, $config->getMaxTries());
    }
}
