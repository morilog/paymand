<?php

namespace Morilog\Paymand\Tests;

use Morilog\Paymand\Config\ConfigBuilder;
use Morilog\Paymand\Contracts\Money;
use Morilog\Paymand\Contracts\RequestNeeds;
use Morilog\Paymand\Paymand;
use PHPUnit\Framework\TestCase;

final class PaymandTest extends TestCase
{
    public function testCreateWithDefaults()
    {
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Not any gateway registered');

        $config = ConfigBuilder::buildDefaults();
        $paymand = new Paymand($config);

        $paymand->request(new RequestNeeds(new Money(1000), 'https://localhost'));
    }
}
