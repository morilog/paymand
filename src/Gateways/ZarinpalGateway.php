<?php

namespace Morilog\Paymand\Gateways;

use Morilog\Paymand\Clients\ZarinpalApiClient;
use Morilog\Paymand\Contracts\Gateway;
use Morilog\Paymand\Contracts\PaymentException;
use Morilog\Paymand\Contracts\RequestNeeds;
use Morilog\Paymand\Contracts\RequestResult;
use Morilog\Paymand\Contracts\VerifyNeeds;
use Morilog\Paymand\Contracts\VerifyResult;

class ZarinpalGateway implements Gateway
{
    /**
     * @var ZarinpalApiClient
     */
    private $client;

    /**
     * @var string
     */
    private $merchantId;

    public function __construct(ZarinpalApiClient $client, string $merchantId)
    {
        $this->client = $client;
        $this->merchantId = $merchantId;
    }

    /**
     * @param RequestNeeds $needs
     * @return RequestResult
     */
    public function request(RequestNeeds $needs): RequestResult
    {
        $result = $this->client->paymentRequest(
            $this->merchantId,
            $needs->getAmount()->getTomans(),
            $needs->get('description'),
            $needs->getCallbackUrl(),
            $needs->get('email'),
            $needs->get('mobile')
        );

        // Successful request
        if (in_array($result->get('status'), [100, 101])) {
            return new RequestResult(
                $this->generatePaymentUrl($result->get('authority')),
                $result->get('authority'),
                $needs->getAmount(),
                $this->getName()
            );
        }

        return $this->handleError($result->get('status'));
    }

    protected function generatePaymentUrl(string $authority)
    {
        return 'https://www.zarinpal.com/pg/StartPay/' . $authority;
    }

    public function getName(): string
    {
        return 'zarinpal';
    }

    private function handleError($statusCode)
    {
        $statuses = (new \ReflectionClass(ZarinpalApiClient::class))->getConstants();
        foreach ($statuses as $name => $code) {
            if ($statusCode === $code) {
                throw new PaymentException($this->getName(), $name, $code);
            }
        }

        throw new PaymentException($this->getName(), 'Unknown error', $statusCode);
    }

    public function verify(VerifyNeeds $needs): VerifyResult
    {
        $result = $this->client->paymentVerification(
            $this->merchantId,
            $needs->getIdentifier(),
            $needs->getAmount()->getTomans()
        );

        // Successful verification
        if (in_array($result->get('status'), [100, 101])) {
            return new VerifyResult(
                $needs->getIdentifier(),
                $result->get('ref_id'),
                $needs->getAmount(),
                $this->getName()
            );
        }

        return $this->handleError($result->get('status'));
    }
}
