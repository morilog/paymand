<?php

namespace Morilog\Paymand\Gateways;

use Assert\Assertion;
use Morilog\Paymand\Clients\SadadApiClient;
use Morilog\Paymand\Contracts\Gateway;
use Morilog\Paymand\Contracts\PaymentException;
use Morilog\Paymand\Contracts\RequestNeeds;
use Morilog\Paymand\Contracts\RequestResult;
use Morilog\Paymand\Contracts\VerifyNeeds;
use Morilog\Paymand\Contracts\VerifyResult;

class SadadGateway implements Gateway
{
    /**
     * @var SadadApiClient
     */
    private $client;

    /**
     * @var string
     */
    private $merchantId;

    /**
     * @var string
     */
    private $terminalId;

    /**
     * @var string
     */
    private $terminalKey;

    public function __construct(SadadApiClient $client, string $merchantId, string $terminalId, string $terminalKey)
    {
        $this->client = $client;
        $this->merchantId = $merchantId;
        $this->terminalId = $terminalId;
        $this->terminalKey = $terminalKey;
    }

    public function request(RequestNeeds $needs): RequestResult
    {
        Assertion::integer($needs->get('order_id'), 'order_id required in requestNeeds');

        $result = $this->client->paymentRequest(
            $this->merchantId,
            $this->terminalId,
            $this->terminalKey,
            $needs->getAmount()->getRials(),
            $needs->get('order_id'),
            new \DateTime(),
            $needs->getCallbackUrl()
        );

        if ($result->get('res_code') === 0) {
            return (new RequestResult(
                $this->generatePaymentUrl($result->get('token')),
                $result->get('token'),
                $needs->getAmount(),
                $this->getName(),
                $result->all()
            ));
        }

        return $this->handleError($result->get('res_code'));
    }

    private function generatePaymentUrl(string $token)
    {
        return sprintf('https://sadad.shaparak.ir/VPG/Purchase?Token=%s', $token);
    }

    public function getName(): string
    {
        return 'sadad';
    }

    public function verify(VerifyNeeds $needs): VerifyResult
    {
        Assertion::integer($needs->get('order_id'), 'order_id required in verifyNeeds');

        $result = $this->client->verify(
            $this->terminalId,
            $needs->get('orderId'),
            $needs->getAmount()->getRials(),
            $needs->getIdentifier(),
            $this->terminalKey
        );

        if ($result->get('res_code') === 0) {
            return new VerifyResult(
                $needs->getIdentifier(),
                $result->get('retrival_ref_no'),
                $needs->getAmount(),
                $this->getName(),
                $result->all()
            );
        }

        return $this->handleError($result->get('res_code'), false);
    }

    private function handleError($statusCode, bool $isRequest = true)
    {
        $allStatuses = (new \ReflectionClass(SadadApiClient::class))->getConstants();
        $prefix = $isRequest ? 'REQUEST_STATUS' : 'VERIFY_STATUS';
        $statuses = array_filter($allStatuses, function ($key) use ($prefix) {
            return starts_with($key, $prefix);
        }, ARRAY_FILTER_USE_KEY);

        foreach ($statuses as $name => $code) {
            if ($statusCode === $code) {
                throw new PaymentException($this->getName(), $name, $code);
            }
        }

        throw new PaymentException($this->getName(), 'Unknown error', $statusCode);
    }
}
