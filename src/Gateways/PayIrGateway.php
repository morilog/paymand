<?php

namespace Morilog\Paymand\Gateways;

use Assert\Assertion;
use Morilog\Paymand\Clients\PayIrApiClient;
use Morilog\Paymand\Contracts\Gateway;
use Morilog\Paymand\Contracts\PaymentException;
use Morilog\Paymand\Contracts\RequestNeeds;
use Morilog\Paymand\Contracts\RequestResult;
use Morilog\Paymand\Contracts\VerifyNeeds;
use Morilog\Paymand\Contracts\VerifyResult;

class PayIrGateway implements Gateway
{
    /**
     * @var PayIrApiClient
     */
    private $client;

    /**
     * @var string
     */
    private $apiKey;

    public function __construct(PayIrApiClient $client, string $apiKey)
    {
        $this->client = $client;
        $this->apiKey = $apiKey;
    }

    public function request(RequestNeeds $needs): RequestResult
    {
        Assertion::notNull($needs->get('mobile'), 'mobile required in requestNeeds');

        $response = $this->client->send(
            $this->apiKey,
            $needs->getAmount()->getRials(),
            $needs->getCallbackUrl(),
            $needs->get('mobile')
        );

        // Successful request
        if ((int)$response->get('status') === 1) {
            return new RequestResult(
                sprintf('https://pay.ir/payment/gateway/%s', $response->get('transId')),
                $response->get('transId'),
                $needs->getAmount(),
                $this->getName()
            );
        }

        return $this->handleError($response->get('status'), true);
    }

    public function getName(): string
    {
        return 'pay_ir';
    }

    public function verify(VerifyNeeds $needs): VerifyResult
    {
        $status = $needs->get('status', 0);

        if ($status === 0) {
            throw PaymentException::unknown($this->getName(), 'payment failed');
        }

        $response = $this->client->verify($this->apiKey, $needs->getIdentifier());

        // Successful Verification
        if ((int)$response->get('status') === 1) {
            return new VerifyResult(
                $needs->getIdentifier(),
                $needs->getIdentifier(),
                $needs->getAmount(),
                $this->getName()
            );
        }

        if ($response->has('status') === false) {
            throw PaymentException::unknown($this->getName(), 'unpredictable exception');
        }

        return $this->handleError($response->get('status'), false);
    }

    private function handleError($statusCode, bool $isRequest = true)
    {
        $allStatuses = (new \ReflectionClass(PayIrApiClient::class))->getConstants();
        $prefix = $isRequest ? 'ERR_REQUEST' : 'ERR_VERIFY';
        $statuses = array_filter($allStatuses, function ($key) use ($prefix) {
            return starts_with($key, $prefix);
        }, ARRAY_FILTER_USE_KEY);

        foreach ($statuses as $name => $code) {
            if ($statusCode === $code) {
                throw new PaymentException($this->getName(), $name, $code);
            }
        }

        throw new PaymentException($this->getName(), 'Unknown error', $statusCode);
    }
}
