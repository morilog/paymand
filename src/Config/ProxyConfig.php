<?php

namespace Morilog\Paymand\Config;

final class ProxyConfig
{
    /**
     * @var bool
     */
    private $enabled;

    /**
     * @var string
     */
    private $host;

    /**
     * @var int
     */
    private $port;

    /**
     * @var bool
     */
    private $isHttps;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    public function __construct(
        bool $enabled = false,
        string $host = '127.0.0.1',
        int $port = 1080,
        bool $isHttps = false,
        string $username = null,
        string $password = null
    ) {
        $this->enabled = $enabled;
        $this->host = $host;
        $this->port = $port;
        $this->isHttps = $isHttps;
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @return int
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @return bool
     */
    public function isHttps()
    {
        return $this->isHttps;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    public function getScheme()
    {
        return $this->isHttps() ? 'https' : 'http';
    }
}
