<?php

namespace Morilog\Paymand\Config;

final class ConfigBuilder
{
    private $proxyConfig;

    private $defaultGateway;

    private $isHAMode;

    private $maxTries;

    private $gateways;

    public function __construct()
    {
        $this->proxyConfig = new ProxyConfig();
        $this->defaultGateway = null;
        $this->isHAMode = false;
        $this->maxTries = 3;
        $this->gateways = [];
    }

    public static function buildDefaults()
    {
        return (new self())->build();
    }

    public function build()
    {
        $gateways = [];
        foreach ($this->gateways as $name => $configs) {
            $gateways[] = new GatewayConfig($name, $configs);
        }

        return new Config($this->proxyConfig, $this->defaultGateway, $this->isHAMode, $this->maxTries, $gateways);
    }

    public static function createBuilder()
    {
        return (new self());
    }

    public function setDefaultGateway(string $name)
    {
        $this->defaultGateway = $name;

        return $this;
    }

    public function enableHighAvailable(int $maxTries = 3)
    {
        $this->isHAMode = true;
        $this->maxTries = $maxTries;

        return $this;
    }

    public function enableProxy()
    {
        $this->proxyConfig = new ProxyConfig(
            true,
            $this->proxyConfig->getHost(),
            $this->proxyConfig->getPort(),
            $this->proxyConfig->isHttps(),
            $this->proxyConfig->getUsername(),
            $this->proxyConfig->getPassword()
        );

        return $this;
    }

    public function setProxyHost(string $host, bool $isHttps = false)
    {
        $this->proxyConfig = new ProxyConfig(
            $this->proxyConfig->isEnabled(),
            $host,
            $this->proxyConfig->getPort(),
            $isHttps,
            $this->proxyConfig->getUsername(),
            $this->proxyConfig->getPassword()
        );

        return $this;
    }

    public function setProxyPort(int $port)
    {
        $this->proxyConfig = new ProxyConfig(
            $this->proxyConfig->isEnabled(),
            $this->proxyConfig->getHost(),
            $port,
            $this->proxyConfig->isHttps(),
            $this->proxyConfig->getUsername(),
            $this->proxyConfig->getPassword()
        );

        return $this;
    }

    public function setProxyAuth(string $username, string $password)
    {
        $this->proxyConfig = new ProxyConfig(
            $this->proxyConfig->isEnabled(),
            $this->proxyConfig->getHost(),
            $this->proxyConfig->getPort(),
            $this->proxyConfig->isHttps(),
            $username,
            $password
        );

        return $this;
    }

    public function withGateway(string $name, array $configs)
    {
        $this->gateways[$name] = $configs;

        return $this;
    }
}
