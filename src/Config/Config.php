<?php

namespace Morilog\Paymand\Config;

final class Config
{
    /**
     * @var ProxyConfig
     */
    private $proxyConfig;

    /**
     * @var string
     */
    private $defaultGateway;

    /**
     * @var bool
     */
    private $highAvailableMode;

    /**
     * @var int
     */
    private $maxTries;

    /**
     * @var GatewayConfig[]
     */
    private $gatewaysConfig;

    public function __construct(
        ProxyConfig $proxyConfig,
        string $defaultGateway = null,
        bool $highAvailableMode = false,
        int $maxTries = 3,
        array $gatewaysConfig = []
    ) {
        $this->proxyConfig = $proxyConfig;
        $this->defaultGateway = $defaultGateway;
        $this->highAvailableMode = $highAvailableMode;
        $this->maxTries = $maxTries;
        $this->gatewaysConfig = $gatewaysConfig;
    }

    /**
     * @return ProxyConfig
     */
    public function getProxyConfig()
    {
        return $this->proxyConfig;
    }

    /**
     * @return string
     */
    public function getDefaultGateway()
    {
        return $this->defaultGateway;
    }

    /**
     * @return bool
     */
    public function isHighAvailableMode()
    {
        return $this->highAvailableMode;
    }

    /**
     * @return int
     */
    public function getMaxTries()
    {
        return $this->maxTries;
    }

    /**
     * @return GatewayConfig[]
     */
    public function getGatewaysConfig()
    {
        return $this->gatewaysConfig;
    }

    public function getGatewayConfig(string $name)
    {
        foreach ($this->gatewaysConfig as $config) {
            if ($config->getName() === $name) {
                return $config;
            }
        }

        return new GatewayConfig($name, []);
    }
}
