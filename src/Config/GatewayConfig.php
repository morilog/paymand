<?php

namespace Morilog\Paymand\Config;

use Morilog\Paymand\Contracts\Payload;

final class GatewayConfig
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var Payload
     */
    private $configs;

    public function __construct(string $name, array $configs)
    {
        $this->name = $name;
        $this->configs = new Payload($configs);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function get(string $key, $default = null)
    {
        return $this->configs->get($key, $default);
    }

    public function isEmpty()
    {
        return empty($this->configs->all());
    }
}
