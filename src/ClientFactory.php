<?php

namespace Morilog\Paymand;

use GuzzleHttp\Client;
use Morilog\Paymand\Config\Config;

final class ClientFactory
{
    /**
     * @var Config
     */
    private $config;

    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    public function soap(string $wsdl)
    {
        $options = ['encoding' => 'UTF-8'];
        $proxyConfig = $this->config->getProxyConfig();

        if ($proxyConfig->isEnabled()) {
            $options['proxy_host'] = $proxyConfig->getHost();
            $options['proxy_port'] = $proxyConfig->getPort();
            $options['stream_context'] = [
                'proxy' => sprintf(
                    "tcp://%s:%s",
                    $proxyConfig->getHost(),
                    $proxyConfig->getPort()
                ),
                'request_fulluri' => true,
            ];
        }
        return new \SoapClient($wsdl, $options);
    }

    public function rest(string $baseUrl = null)
    {
        $proxyConfig = $this->config->getProxyConfig();
        $options = $baseUrl !== null ?['base_uri' => $baseUrl] : [];

        if ($proxyConfig->isEnabled()) {
            $options['proxy'] = [
                $proxyConfig->getScheme() => sprintf(
                    "tcp://%s:%s",
                    $proxyConfig->getHost(),
                    $proxyConfig->getPort()
                ),
            ];
        }

        return new Client($options);
    }
}
