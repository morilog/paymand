<?php

namespace Morilog\Paymand;

use Morilog\Paymand\Clients\PayIrApiClient;
use Morilog\Paymand\Clients\SadadApiClient;
use Morilog\Paymand\Clients\ZarinpalApiClient;
use Morilog\Paymand\Config\Config;
use Morilog\Paymand\Config\GatewayConfig;
use Morilog\Paymand\Gateways\PayIrGateway;
use Morilog\Paymand\Gateways\SadadGateway;
use Morilog\Paymand\Gateways\ZarinpalGateway;

final class GatewayFactory
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @var ClientFactory
     */
    private $clientFactory;

    public function __construct(Config $config, ClientFactory $clientFactory)
    {
        $this->config = $config;
        $this->clientFactory = $clientFactory;
    }

    public function make(string $name)
    {
        $config = $this->config->getGatewayConfig($name);
        switch ($name) {
            case 'zarinpal':
                return  $this->makeZarinpal($config);
            case 'pay_ir':
                return $this->makePayIr($config);
            case 'sadad':
                return $this->makeSadad($config);

            default:
                throw new \InvalidArgumentException('Not supported gateway');
        }
    }
    
    private function makeZarinpal(GatewayConfig $config)
    {
        return new ZarinpalGateway(new ZarinpalApiClient($this->clientFactory), $config->get('merchant_id'));
    }

    private function makePayIr(GatewayConfig $config)
    {
        return new PayIrGateway(new PayIrApiClient($this->clientFactory), $config->get('api_key'));
    }

    private function makeSadad(GatewayConfig $config)
    {
        return new SadadGateway(
            new SadadApiClient($this->clientFactory),
            $config->get('merchant_id'),
            $config->get('terminal_id'),
            $config->get('terminal_key')
        );
    }
}
