<?php

namespace Morilog\Paymand;

use Morilog\Paymand\Contracts\Gateway;

final class RegisteredGateway
{
    /**
     * @var Gateway
     */
    private $gateway;

    /**
     * @var int
     */
    private $priority;

    public function __construct(Gateway $gateway, int $priority)
    {
        $this->gateway = $gateway;
        $this->priority = $priority;
    }

    /**
     * @return Gateway
     */
    public function getGateway()
    {
        return $this->gateway;
    }

    /**
     * @return int
     */
    public function getPriority()
    {
        return $this->priority;
    }
}
