<?php

namespace Morilog\Paymand;

use Morilog\Paymand\Config\Config;
use Morilog\Paymand\Contracts\Gateway;
use Morilog\Paymand\Contracts\PaymentException;
use Morilog\Paymand\Contracts\RequestNeeds;
use Morilog\Paymand\Contracts\VerifyNeeds;

class Paymand
{
    /**
     * @var RegisteredGateway[]
     */
    private $gateways;

    /**
     * @var string
     */
    private $default;

    /**
     * @var bool
     */
    private $highAvailable;

    /**
     * @var int
     */
    private $maxTries;

    /**
     * @var Config
     */
    private $config;

    public function __construct(Config $config)
    {
        $this->highAvailable = $config->isHighAvailableMode();
        $this->maxTries = $config->getMaxTries();
        $this->default = $config->getDefaultGateway();
        $this->gateways = [];
        $this->config = $config;
    }

    public function addGateway(Gateway $gateway, int $priority = 1)
    {
        $this->gateways[$gateway->getName()] = new RegisteredGateway($gateway, $priority);

        if ($this->default === null) {
            $this->default = $gateway->getName();
        }

        uasort($this->gateways, function (RegisteredGateway $registered) {
            return $registered->getPriority();
        });

        return $this;
    }

    public function extend(string $name, \Closure $resolver)
    {
        $config = $this->config->getGatewayConfig($name);
        $gateway = $resolver($config);

        if (!$gateway instanceof Gateway) {
            throw new \RuntimeException('Invalid gateway resolver');
        }

        return $this->addGateway($gateway, (int)$config->get('priority', 1));
    }

    public function choose(string $gateway)
    {
        $this->default = $gateway;

        return $this;
    }

    private function getRegisteredGatewayByName(string $name)
    {
        foreach ($this->gateways as $key => $registeredGateway) {
            if ($key === $name) {
                return $registeredGateway;
            }
        }

        throw new \InvalidArgumentException(sprintf('Gateway %s not registered', $name));
    }

    public function request(RequestNeeds $needs)
    {
        if ($this->isHighAvailableMode()) {
            return $this->requestInHAMode($needs);
        }

        $gateway = $this->getDefault();

        try {
            return $gateway->request($needs);
        } catch (\Exception $e) {
            if (!$e instanceof PaymentException) {
                $e = new PaymentException($gateway->getName(), $e->getMessage(), $e->getCode());
            }

            throw $e;
        }
    }

    private function isHighAvailableMode()
    {
        return $this->highAvailable;
    }

    private function requestInHAMode(RequestNeeds $needs)
    {
        $maxTries = $this->maxTries;
        $gateways = $this->getGatewaysByPriority($maxTries);
        $maxTries = count($gateways);
        $tries = 1;
        foreach ($gateways as $gateway) {
            try {
                return $gateway->request($needs);
            } catch (\Exception $e) {
                if ($tries < $maxTries) {
                    $tries++;
                    continue;
                }

                if (!$e instanceof PaymentException) {
                    $e = new PaymentException($gateway->getName(), $e->getMessage(), $e->getCode());
                }

                throw $e;
            }
        }
    }

    /**
     * @param int $max
     * @return Gateway[]
     */
    private function getGatewaysByPriority(int $max)
    {
        if (empty($this->gateways)) {
            throw new \RuntimeException('Not any gateway registered');
        }

        return array_map(function (RegisteredGateway $gateway) {
            return $gateway->getGateway();
        }, array_slice($this->gateways, 0, $max));
    }

    public function getDefault()
    {
        if (empty($this->gateways)) {
            throw new \RuntimeException('Not any gateway registered');
        }

        return $this->getRegisteredGatewayByName($this->default)->getGateway();
    }

    public function verify(VerifyNeeds $needs)
    {
        $gateway = $this->getDefault();
        try {
            return $gateway->verify($needs);
        } catch (\Exception $e) {
            if (!$e instanceof PaymentException) {
                $e = PaymentException::unknown($gateway->getName(), $e->getMessage());
            }

            throw $e;
        }
    }
}
