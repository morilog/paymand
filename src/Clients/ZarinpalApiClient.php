<?php

namespace Morilog\Paymand\Clients;

use Assert\Assertion;
use Morilog\Paymand\ClientFactory;
use Morilog\Paymand\Contracts\GatewayApiClient;
use Morilog\Paymand\Contracts\GatewayClientException;
use Morilog\Paymand\Contracts\Payload;

/**
 * Class ZarinpalClient
 * @package Morilog\Paymand\Clients
 */
final class ZarinpalApiClient implements GatewayApiClient
{
    const STATUS_INCOMPLETE_PAYLOAD = -1;
    const STATUS_NOT_AUTHORIZED = -2;
    const STATUS_AMOUNT_IS_TOO_MUCH = -3;
    const STATUS_LOW_VENDOR_LEVEL = -4;
    const STATUS_REQUEST_NOT_FOUND = -11;
    const STATUS_REQUEST_NOT_EDITABLE = -12;
    const STATUS_UNDEFINED_REQUEST = -21;
    const STATUS_TRANSACTION_FAILED = -22;
    const STATUS_AMOUNT_MISMATCH = -33;
    const STATUS_TRANSACTION_OVER_FLOW = -34;
    const STATUS_NOT_AUTHORIZED_METHOD = -40;
    const STATUS_INVALID_ADDITIONAL_DATA = -41;
    const STATUS_PAYMENT_ID_EXPIRED = -42;
    const STATUS_REQUEST_ARCHIVED = -54;
    const STATUS_TRANSACTION_SUCCESSFUL = 100;
    const STATUS_TRANSACTION_SUCCESSFUL_BEFORE = 101;

    const WSDL_URL = 'https://www.zarinpal.com/pg/services/WebGate/wsdl';

    /**
     * @var \SoapClient
     */
    private $client;

    public function __construct(ClientFactory $factory)
    {
        $this->client = $factory->soap(self::WSDL_URL);
    }

    /**
     * @param string $merchantId
     * @param int $amount
     * @param string $description
     * @param string $callbackUrl
     * @param string $email
     * @param string $mobile
     * @return Payload
     */
    public function paymentRequest(
        string $merchantId,
        int $amount,
        string $description,
        string $callbackUrl,
        string $email = '',
        string $mobile = ''
    ) {
        try {
            Assertion::length($merchantId, 36);
            Assertion::url($callbackUrl);

            $result = $this->client->PaymentRequest([
                'MerchantID' => $merchantId,
                'Amount' => $amount,
                'Description' => $description,
                'Email' => $email,
                'Mobile' => $mobile,
                'CallbackURL' => $callbackUrl,
            ]);

            return new Payload([
                'status' => (int)$result->Status,
                'authority' => $result->Authority,
            ]);
        } catch (\Exception $e) {
            throw new GatewayClientException($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param string $merchantId
     * @param string $authority
     * @param int $amount
     * @return Payload
     */
    public function paymentVerification(string $merchantId, string $authority, int $amount)
    {
        try {
            Assertion::length($merchantId, 36);

            $result = $this->client->PaymentVerification([
                'MerchantID' => $merchantId,
                'Authority' => $authority,
                'Amount' => $amount,
            ]);

            return new Payload([
                'status' => (int)$result->Status,
                'ref_id' => $result->RefID,
            ]);
        } catch (\Exception $e) {
            throw new GatewayClientException($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param string $merchantId
     * @param int $amount
     * @param string $description
     * @param string $callbackUrl
     * @param array $additionalData
     * @param string $email
     * @param string $mobile
     * @return Payload
     */
    public function paymentRequestWithExtra(
        string $merchantId,
        int $amount,
        string $description,
        string $callbackUrl,
        array $additionalData,
        string $email = '',
        string $mobile = ''
    ) {
        try {
            $result = $this->client->PaymentRequestWithExtra([
                'MerchantID' => $merchantId,
                'Amount' => $amount,
                'Description' => $description,
                'Email' => $email,
                'Mobile' => $mobile,
                'CallbackURL' => $callbackUrl,
                'AdditionalData' => json_encode($additionalData),
            ]);

            return new Payload([
                'status' => (int)$result->Status,
                'authority' => $result->Authority,
            ]);
        } catch (\Exception $e) {
            throw new GatewayClientException($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param string $merchantId
     * @param string $authority
     * @param int $amount
     * @return Payload
     */
    public function paymentVerificationWithExtra(string $merchantId, string $authority, int $amount)
    {
        try {
            Assertion::length($merchantId, 36);

            $result = $this->client->PaymentVerificationWithExtra([
                'MerchantID' => $merchantId,
                'Authority' => $authority,
                'Amount' => $amount,
            ]);

            return new Payload([
                'status' => (int)$result->Status,
                'ref_id' => $result->RefID,
                'extra_detail' => json_decode($result->ExtraDetail, true),
            ]);
        } catch (\Exception $e) {
            throw new GatewayClientException($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param string $merchantId
     * @param string $authority
     * @param int $expireIn
     * @return Payload
     */
    public function refreshAuthority(string $merchantId, string $authority, int $expireIn)
    {
        try {
            Assertion::length($merchantId, 36);
            Assertion::greaterThan($expireIn, 0);

            $result = $this->client->RefreshAuthority([
                'MerchantID' => $merchantId,
                'Authority' => $authority,
                'ExpireIn' => $expireIn,
            ]);

            return new Payload([
                'status' => (int)$result->Status,
            ]);
        } catch (\Exception $e) {
            throw new GatewayClientException($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param string $merchantId
     * @return Payload
     */
    public function getUnverifiedTransactions(string $merchantId)
    {
        try {
            $result = $this->client->GetUnverifiedTransactions(['MerchantID' => $merchantId]);

            return new Payload([
                'status' => (int)$result->Status,
                'authorities' => json_decode($result->Authoroties, true),
            ]);
        } catch (\Exception $e) {
            throw new GatewayClientException($e->getMessage(), $e->getCode());
        }
    }
}
