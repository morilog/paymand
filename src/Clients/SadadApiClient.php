<?php

namespace Morilog\Paymand\Clients;

use GuzzleHttp\Client;
use Morilog\Paymand\ClientFactory;
use Morilog\Paymand\Contracts\GatewayApiClient;
use Morilog\Paymand\Contracts\GatewayClientException;
use Morilog\Paymand\Contracts\Payload;

final class SadadApiClient implements GatewayApiClient
{
    const REQUEST_STATUS_SUCCESSFUL = 0;
    const REQUEST_STATUS_INVALID_MERCHANT = 3;
    const REQUEST_STATUS_INACTIVE_MERCHANT = 23;
    const REQUEST_STATUS_VENDOR_NOT_AUTHORIZED = 58;
    const REQUEST_STATUS_TOO_MUCH_AMOUNT = 61;
    const REQUEST_STATUS_WRONG_PARAMETERS_ORDER = 1000;
    const REQUEST_STATUS_WRONG_PARAMETERS_VALUE = 1001;
    const REQUEST_STATUS_ERROR_IN_SYSTEM = 1002;
    const REQUEST_STATUS_INVALID_IP_CONTACT_BANK = 1003;
    const REQUEST_STATUS_INVALID_ACCEPTOR_NUMBER_CONTACT_BANK = 1004;
    const REQUEST_STATUS_ACCESS_DENIED = 1005;
    const REQUEST_STATUS_ERROR_IN_GATEWAY = 1006;
    const REQUEST_STATUS_DUPLICATE_ORDER_ID = 1011;
    const REQUEST_STATUS_WRONG_TRANSACTION_KEY = 1012;
    const REQUEST_STATUS_UNKNOWN_ERROR = 1015;
    const REQUEST_STATUS_AMOUNT_MUCH_THAN_LIMIT = 1017;
    const REQUEST_STATUS_WRONG_SERVER_TIME = 1018;
    const REQUEST_STATUS_NOT_ALLOWED_SHETAB = 1019;
    const REQUEST_STATUS_ACCEPTOR_DISABLED = 1020;
    const REQUEST_STATUS_INVALID_CALLBACK_URL = 1023;
    const REQUEST_STATUS_INVALID_TIMESTAMP = 1024;
    const REQUEST_STATUS_INVALID_SIGNATURE = 1025;
    const REQUEST_STATUS_INVALID_ORDER_ID = 1026;
    const REQUEST_STATUS_INVALID_ACCEPTOR_NUMBER = 1027;
    const REQUEST_STATUS_INVALID_TERMINAL_ID = 1028;
    const REQUEST_STATUS_INVALID_IP = 1029;
    const REQUEST_STATUS_INVALID_DOMAIN_ADDRESS = 1030;
    const REQUEST_STATUS_PAYMENT_DURATION_EXPIRED = 1031;
    const REQUEST_STATUS_ACCEPTOR_DISABLED_BECAUSE_OF_ERROR = 1032;
    const REQUEST_STATUS_INVALID_ADDITIONAL_DATA = 1036;
    const REQUEST_STATUS_INVALID_MERCHANT_ID_OR_TERMINAL_ID = 1037;
    const REQUEST_STATUS_INVALID_REQUEST_BY_ACCEPTOR = 1053;
    const REQUEST_STATUS_INVALID_DATA = 1055;
    const REQUEST_STATUS_TEMPORARY_SYSTEM_DISABLED = 1056;
    const REQUEST_STATUS_OUT_OF_SERVICE = 1058;
    const REQUEST_STATUS_ERROR_ON_GENERATING_UNIQUE_CODE = 1061;
    const REQUEST_STATUS_TRY_AGAIN = 1064;
    const REQUEST_STATUS_CONNECTION_FAILED = 1065;
    const REQUEST_STATUS_SERVICE_TEMPORARY_UNAVAILABLE = 1066;
    const REQUEST_STATUS_SERVICE_TEMPORARY_UNAVAILABLE_BECAUSE_OF_UPDATE = 1068;
    const REQUEST_STATUS_INVALID_AMOUNT = 1101;
    const REQUEST_STATUS_INVALID_TOKEN = 1103;
    const REQUEST_STATUS_INVALID_MULTIPLEXING_DATA = 1104;

    const VERIFY_STATUS_SUCCESSFUL = 0;
    const VERIFY_STATUS_INVALID_PARAMETERS = -1;
    const VERIFY_STATUS_TRANSACTION_EXPIRED = 101;


    const BASE_API_URL = 'https://sadad.shaparak.ir';

    /**
     * @var Client
     */
    private $client;

    public function __construct(ClientFactory $factory)
    {
        $this->client = $factory->rest(self::BASE_API_URL);
    }

    public function paymentRequest(
        string $merchantId,
        string $terminalId,
        string $terminalKey,
        int $amount,
        int $orderId,
        \DateTimeInterface $localDateTime,
        string $returnUrl,
        array $additionalData = [],
        array $multiplexingData = []
    ) {
        try {
            $result = $this->client->request('POST', '/vpg/api/v0/Request/PaymentRequest', [
                'form_params' => [
                    'MerchantId' => $merchantId,
                    'TerminalId' => $terminalId,
                    'Amount' => $amount,
                    'OrderId' => $orderId,
                    'LocalDateTime' => $localDateTime->format('m/d/Y g:i:s a'),
                    'ReturnUrl' => $returnUrl,
                    'SignData' => static::createSignData($terminalId, $orderId, $amount, $terminalKey),
                    'AdditionalData' => json_encode($additionalData),
                    'MultiplexingData' => json_encode($multiplexingData),
                ]
            ]);

            $result = json_decode($result->getBody()->getContents(), true);

            return new Payload([
                'res_code' => (int)array_get($result, 'ResCode'),
                'token' => array_get($result, 'Token'),
                'description' => array_get($result, 'Description'),
            ]);
        } catch (\Exception $e) {
            throw new GatewayClientException($e->getMessage(), $e->getCode());
        }
    }

    public function verify(string $terminalId, int $orderId, int $amount, string $token, string $terminalKey)
    {
        try {
            $result = $this->client->request('POST', '/VPG/api/v0/Advice/Verify', [
                'form_params' => [
                    'Token' => $token,
                    'SignData' => static::createSignData($terminalId, $orderId, $amount, $terminalKey)
                ]
            ]);

            $result = json_decode($result->getBody()->getContents(), true);

            return new Payload([
                'res_code' => (int)array_get($result, 'ResCode'),
                'amount' => array_get($result, 'Amount'),
                'description' => array_get($result, 'Description'),
                'retrival_ref_no' => array_get($result, 'RetrivalRefNo'),
                'system_trace_no' => array_get($result, 'SystemTraceNo'),
                'order_id' => array_get($result, 'OrderId')
            ]);
        } catch (\Exception $e) {
            throw new GatewayClientException($e->getMessage(), $e->getCode());
        }
    }

    public static function createSignData(string $terminalId, int $orderId, int $amount, string $terminalKey)
    {
        $method = 'DES-EDE3';
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length($method));
        return openssl_encrypt(
            sprintf('%s;%s;%s', $terminalId, $orderId, $amount),
            $method,
            $terminalKey,
            0,
            $iv
        );
    }
}
