<?php

namespace Morilog\Paymand\Clients;

use Assert\Assertion;
use GuzzleHttp\Client;
use Morilog\Paymand\ClientFactory;
use Morilog\Paymand\Contracts\GatewayApiClient;
use Morilog\Paymand\Contracts\GatewayClientException;
use Morilog\Paymand\Contracts\Payload;

final class PayIrApiClient implements GatewayApiClient
{
    // Request errors
    const ERR_REQUEST_API_REQUIRED = -1;
    const ERR_REQUEST_AMOUNT_REQUIRED = -2;
    const ERR_REQUEST_AMOUNT_MUST_BE_INTEGER = -3;
    const ERR_REQUEST_AMOUNT_NOT_ENOUGH = -4;
    const ERR_REQUEST_REDIRECT_URL_REQUIRED = -5;
    const ERR_REQUEST_API_NOT_AUTHORIZED = -6;
    const ERR_REQUEST_VENDOR_IS_DISABLED = -7;
    const ERR_REQUEST_REDIRECT_URL_MISMATCH = -8;
    const ERR_REQUEST_DESCRIPTION_TOO_LONG = -12;
    const ERR_REQUEST_FAILED = 'failed';

    // Verify errors
    const ERR_VERIFY_TRANS_ID_REQUIRED = -2;
    const ERR_VERIFY_API_NOT_AUTHORIZED = -3;
    const ERR_VERIFY_VENDOR_IS_DISABLED = -4;
    const ERR_VERIFY_FAILED = -5;

    const API_BASE_URL = 'https://pay.ir';

    /**
     * @var Client
     */
    private $client;

    public function __construct(ClientFactory $factory)
    {
        $this->client = $factory->rest(self::API_BASE_URL);
    }

    public function send(
        string $api,
        int $amount,
        string $redirect,
        string $mobile,
        string $factorNumber = null,
        string $description = null
    ) {
        try {
            Assertion::greaterOrEqualThan($amount, 1000);
            Assertion::url($redirect);
            if ($description !== null) {
                Assertion::maxLength($description, 255);
            }
            $response = $this->client->request('POST', '/payment/send', [
                'form_params' => [
                    'api' => $api,
                    'amount' => $amount,
                    'redirect' => urlencode($redirect),
                    'factorNumber' => $factorNumber,
                    'mobile' => $mobile,
                ],
            ]);

            $response = json_decode($response->getBody()->getContents(), true);

            return new Payload([
                'status' => $response['status'],
                'transId' => $response['transId']
            ]);
        } catch (\Exception $e) {
            throw new GatewayClientException($e->getMessage(), $e->getCode());
        }
    }

    public function verify(string $api, string $transId)
    {
        try {
            $response = $this->client->request('POST', '/payment/verify', [
                'form_params' => [
                    'api' => $api,
                    'transId' => $transId
                ]
            ]);

            return new Payload(json_decode($response->getBody()->getContents(), true));
        } catch (\Exception $e) {
            throw new GatewayClientException($e->getMessage(), $e->getCode());
        }
    }
}
