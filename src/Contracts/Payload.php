<?php

namespace Morilog\Paymand\Contracts;

use Assert\Assertion;

class Payload
{
    /**
     * @var array
     */
    private $pairs;

    public function __construct(array $pairs = [])
    {
        $this->pairs = $pairs;
    }

    public function get(string $key, $default = null)
    {
        return $this->has($key) ? $this->pairs[$key] : $default;
    }

    public function getOrException(string $key)
    {
        Assertion::keyExists($this->all(), $key);

        return $this->get($key);
    }

    public function put(string $key, $value)
    {
        return new static(array_merge($this->all(), [$key => $value]));
    }

    public function has(string $key)
    {
        return array_key_exists($key, $this->all());
    }

    public function all()
    {
        return $this->pairs;
    }
}
