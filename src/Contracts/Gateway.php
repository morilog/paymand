<?php

namespace Morilog\Paymand\Contracts;

interface Gateway
{
    public function request(RequestNeeds $needs): RequestResult;

    public function verify(VerifyNeeds $needs): VerifyResult;

    public function getName(): string;
}
