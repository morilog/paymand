<?php

namespace Morilog\Paymand\Contracts;

final class VerifyResult
{
    /**
     * @var Payload
     */
    private $payload;

    public function __construct(
        string $identifier,
        string $referenceCode,
        Money $amount,
        string $gatewayName,
        array $extra = []
    ) {
        $this->payload = new Payload(array_merge($extra, [
            'identifier' => $identifier,
            'reference_code' => $referenceCode,
            'gateway_name' => $gatewayName,
            'amount' => $amount,
        ]));
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        return $this->get('identifier');
    }

    /**
     * @return string
     */
    public function getReferenceCode()
    {
        return $this->get('reference_code');
    }

    /**
     * @return string
     */
    public function getGatewayName()
    {
        return $this->get('gateway_name');
    }

    /**
     * @return Money
     */
    public function getAmount()
    {
        return $this->get('amount');
    }

    public function get(string $key, $default = null)
    {
        return $this->payload->get($key, $default);
    }

    public function getExtra()
    {
        return $this->payload->all();
    }
}
