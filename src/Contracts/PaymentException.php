<?php

namespace Morilog\Paymand\Contracts;

class PaymentException extends \RuntimeException
{
    const CODE_UNKNOWN = 10000;

    /**
     * @var string
     */
    private $gateway;

    public function __construct(string $gateway, string $message, int $code)
    {
        parent::__construct($message, $code, null);
        $this->gateway = $gateway;
    }

    /**
     * @return string
     */
    public function getGateway()
    {
        return $this->gateway;
    }

    public static function unknown(string $gateway, string $message)
    {
        return new self($gateway, $message, self::CODE_UNKNOWN);
    }
}
