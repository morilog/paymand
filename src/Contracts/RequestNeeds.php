<?php

namespace Morilog\Paymand\Contracts;

final class RequestNeeds
{
    /**
     * @var Payload
     */
    private $payload;

    public function __construct(Money $amount, string $callbackUrl, array $extra = [])
    {
        $this->payload = new Payload(array_merge($extra, [
            'amount' => $amount,
            'callback_url' => $callbackUrl
        ]));
    }

    public function getAmount(): Money
    {
        return $this->get('amount');
    }

    public function getCallbackUrl()
    {
        return $this->get('callback_url');
    }

    public function with(string $key, $value)
    {
        return new self(
            $this->getAmount(),
            $this->getCallbackUrl(),
            array_merge($this->payload->all(), [$key => $value])
        );
    }

    public function getExtra()
    {
        return $this->payload->all();
    }

    public function get(string $key, $default = null)
    {
        return $this->payload->get($key, $default);
    }
}
