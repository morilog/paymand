<?php

namespace Morilog\Paymand\Contracts;

final class RequestResult
{
    /**
     * @var Payload
     */
    private $payload;

    public function __construct(string $paymentUrl, string $identifier, Money $amount, string $gatewayName, array $extra = [])
    {
        $this->payload = new Payload(array_merge($extra, [
            'payment_url' => $paymentUrl,
            'identifier' => $identifier,
            'amount' => $amount,
            'gateway_name' => $gatewayName
        ]));
    }

    /**
     * @return string
     */
    public function getPaymentUrl()
    {
        return $this->payload->get('payment_url');
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        return $this->payload->get('identifier');
    }

    /**
     * @return Money
     */
    public function getAmount()
    {
        return $this->payload->get('amount');
    }

    /**
     * @return string
     */
    public function getGatewayName()
    {
        return $this->payload->get('gateway_name');
    }

    public function get(string $key, $default = null)
    {
        return $this->payload->get($key, $default);
    }

    public function getExtra()
    {
        return $this->payload->all();
    }
}
