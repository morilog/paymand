<?php

namespace Morilog\Paymand\Contracts;

final class VerifyNeeds
{
    /**
     * @var Payload
     */
    private $payload;

    public function __construct(Money $amount, string $identifier, array $extra = [])
    {
        $this->payload = new Payload(array_merge($extra, [
            'amount' => $amount,
            'identifier' => $identifier
        ]));
    }

    public function getAmount(): Money
    {
        return $this->get('amount');
    }

    public function getIdentifier()
    {
        return $this->get('identifier');
    }

    public function with(string $key, $value)
    {
        return new self(
            $this->getAmount(),
            $this->getIdentifier(),
            array_merge($this->payload->all(), [$key => $value])
        );
    }

    public function getExtra()
    {
        return $this->payload->all();
    }

    public function get(string $key, $default = null)
    {
        return $this->payload->get($key, $default);
    }
}
