<?php

namespace Morilog\Paymand\Contracts;

use Assert\Assertion;

final class Money
{
    /**
     * @var int
     */
    private $amount;

    public function __construct(int $amount)
    {
        Assertion::greaterOrEqualThan($amount, 1000);

        $this->amount = $amount;
    }

    public function getValue()
    {
        return $this->amount;
    }

    public function __toString()
    {
        return (string)$this->getValue();
    }

    public static function fromTomans(int $tomans)
    {
        Assertion::greaterOrEqualThan($tomans, 100);
        return new self($tomans * 10);
    }

    public function getTomans()
    {
        return $this->getValue() / 10;
    }

    public function getRials()
    {
        return $this->getValue();
    }

    public function equalsTo(Money $money)
    {
        return $this->getValue() === $money->getValue();
    }
}
