<?php

if (!function_exists('array_get')) {
    function array_get(array $arr, string $key, $default = null)
    {
        $value = $arr;
        $depths = explode('.', $key);

        foreach ($depths as $depth) {
            if (isset($value[$depth]) === false) {
                return $default;
            }

            $value = $value[$depth];
        }

        return $value;
    }
}

if (!function_exists('starts_with')) {
    function starts_with($haystack, $needles)
    {
        foreach ((array)$needles as $needle) {
            if ($needle !== '' && substr($haystack, 0, strlen($needle)) === (string)$needle) {
                return true;
            }
        }
        return false;
    }
}
