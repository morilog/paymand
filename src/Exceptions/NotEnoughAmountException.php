<?php

namespace Morilog\Paymand\Exceptions;

use Morilog\Paymand\Contracts\PaymentException;

final class NotEnoughAmountException extends PaymentException
{
    public function __construct(string $gateway)
    {
        parent::__construct($gateway, 'amount is not enough', self::CODE_NOT_ENOUGH_AMOUNT);
    }
}
