<?php

namespace Morilog\Paymand\Exceptions;

use Morilog\Paymand\Contracts\PaymentException;

final class AmountMismatchException extends PaymentException
{
    public function __construct(string $gateway)
    {
        parent::__construct($gateway, 'amount mismatch', self::CODE_AMOUNT_MISMATCH);
    }
}
