<?php

namespace Morilog\Paymand\Exceptions;

use Morilog\Paymand\Contracts\PaymentException;

final class InCompletePayloadException extends PaymentException
{
    public function __construct(string $gateway)
    {
        parent::__construct($gateway, 'incomplete payload', self::CODE_INCOMPLETE_PAYLOAD);
    }
}
