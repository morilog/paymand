<?php

namespace Morilog\Paymand\Exceptions;

use Morilog\Paymand\Contracts\PaymentException;

final class InvalidApiKeyException extends PaymentException
{
    public function __construct(string $gateway)
    {
        parent::__construct($gateway, 'invalid api key', self::CODE_INVALID_API_KEY);
    }
}
