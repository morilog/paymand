<?php

namespace Morilog\Paymand\Exceptions;

use Morilog\Paymand\Contracts\PaymentException;

final class InvalidRequestException extends PaymentException
{
    public function __construct(string $gateway)
    {
        parent::__construct($gateway, 'invalid request', self::CODE_INVALID_REQUEST);
    }
}
